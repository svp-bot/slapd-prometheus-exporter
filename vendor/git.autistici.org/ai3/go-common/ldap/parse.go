package ldaputil

import (
	"fmt"

	"github.com/go-ldap/ldap/v3"
)

// ParseScope parses a string representation of an LDAP scope into the
// proper enum value.
func ParseScope(s string) (int, error) {
	switch s {
	case "base":
		return ldap.ScopeBaseObject, nil
	case "one":
		return ldap.ScopeSingleLevel, nil
	case "sub":
		return ldap.ScopeWholeSubtree, nil
	default:
		return 0, fmt.Errorf("unknown LDAP scope '%s'", s)
	}
}
