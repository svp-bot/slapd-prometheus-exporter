module git.autistici.org/ai3/tools/slapd-prometheus-exporter

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/prometheus/client_golang v1.9.0
)
