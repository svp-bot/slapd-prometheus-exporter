slapd-prometheus-exporter
=========================

A reasonably useful Prometheus exported for OpenLDAP *slapd*. It tries
to run minimal LDAP queries so that it works on large/busy
installations.

# Usage

The exporter is a standalone daemon meant to be run alongside
*slapd*. It runs its own HTTP server to serve the
Prometheus-compatible */metrics* endpoint. To run it:

    $ slapd-prometheus-exporter [--addr=ADDR] [--uri=URI] [--base=DN] &

Where *ADDR* is the `[HOST]:PORT` address to bind to, *URI* is the
LDAP URI of the slapd server (by default it will try to connect on the
standard LDAPI UNIX socket). If --base is specified, *DN* should point
at the root object of your database, which will export its
*contextCSN* attribute, useful for monitoring of replication delay.
